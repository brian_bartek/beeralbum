package com.example.chello.beeralbum;

import android.provider.BaseColumns;

/**
 * Created by Chello on 2015-06-23.
 */
public class BeerAlbumContract {

    public BeerAlbumContract() {};

    // table contents
    public static abstract class BeerEntry implements BaseColumns {
        public static final String TABLE_NAME = "beer";
//        public static final String COLUMN_NAME_BEER_ID = "beerid";
        public static final String COLUMN_NAME_BEER_NAME = "name";
        public static final String COLUMN_NAME_BEER_DESCRIPTION = "description";
        public static final String COLUMN_NAME_BEER_IMAGE_URL = "url";

    }
}
