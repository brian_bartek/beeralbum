package com.example.chello.beeralbum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Chello on 2015-06-23.
 */
public class BeerAlbumDbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "BeerAlbum.db";
    private static final String SQL_CREATE_TABLE =
            "create table " + BeerAlbumContract.BeerEntry.TABLE_NAME +
                    " (" + BeerAlbumContract.BeerEntry._ID + " INTEGER PRIMARY KEY," +
                    BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_NAME + " TEXT unique," +
                    BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_DESCRIPTION + " TEXT," +
                    BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_IMAGE_URL + " TEXT)";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + BeerAlbumContract.BeerEntry.TABLE_NAME;

    public BeerAlbumDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
        closeDB();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
        closeDB();
    }

    public long putBeer(Context context, Beer beerItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_NAME, beerItem.getName());
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_DESCRIPTION, beerItem.getDescription());
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_IMAGE_URL, beerItem.getImageUrl());

        long newRowId = -1;
        try {
            newRowId = db.insertOrThrow(BeerAlbumContract.BeerEntry.TABLE_NAME, null, values);
        } catch (SQLiteConstraintException e) {
            System.out.println("DB ERROR PUT: " + e.toString());
            Toast.makeText(context, "Beer with this name already exists!", Toast.LENGTH_LONG).show();
        }
        return newRowId;
    }

    public boolean updateBeer(String oldBeerName, Beer beerItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_NAME, beerItem.getName());
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_DESCRIPTION, beerItem.getDescription());
        values.put(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_IMAGE_URL, beerItem.getImageUrl());

        // zamiast dodawa� id do klasy, bazy danych, mo�na ustawi�, �eby nazwa piwa by�a unikalna i na jej podstawie podmienia�
        try {
            int affRows = db.update(BeerAlbumContract.BeerEntry.TABLE_NAME, values, "name = ?", new String[]{oldBeerName});
            System.out.println("DB UPDATE: beers affected: " + affRows);
        } catch (Exception e) {
            System.out.println("DB ERROR UPDATE: " + e.toString());
        }
        db.close();
        return true;
    }

    public boolean deleteBeer(String oldBeerName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(BeerAlbumContract.BeerEntry.TABLE_NAME, "name = ?", new String[]{oldBeerName});
        return true;
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public ArrayList<Beer> getBeersList() {
        ArrayList<Beer> beerList = new ArrayList<Beer>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + BeerAlbumContract.BeerEntry.TABLE_NAME, null);
        if (!cursor.moveToFirst()) return new ArrayList<Beer>();
        do {
            Beer tempBeer = new Beer();
            tempBeer.setName(cursor.getString(cursor.getColumnIndex(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_NAME)));
            tempBeer.setDescription(cursor.getString(cursor.getColumnIndex(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_DESCRIPTION)));
            tempBeer.setImageUrl(cursor.getString(cursor.getColumnIndex(BeerAlbumContract.BeerEntry.COLUMN_NAME_BEER_IMAGE_URL)));
            beerList.add(tempBeer);
        } while (cursor.moveToNext());
        return beerList;
    }
}
