package com.example.chello.beeralbum;

/**
 * Created by Chello on 2015-06-23.
 */
public class Beer {
    private String name;
    private String description;
    private String imageUrl;

    public Beer() {
        name = "beer name"; description = "desc"; imageUrl=null;}

    public Beer(String name, String description, String imageUrl) {
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
