package com.example.chello.beeralbum;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class SingleBeerActivity extends Activity{
    EditText beerName;
    EditText beerDescription;
    ImageView beerUrl;
    private BeerAlbumDbHelper mydb;
    private int beerId;
    Beer tempBeer;
    String oldBeerName;
    String beerIconUrl = "@drawable/beericon";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_beer);
        mydb = new BeerAlbumDbHelper(this);
        beerId = -1;

        beerName = (EditText) findViewById(R.id.activityBeerName);
        beerDescription = (EditText) findViewById(R.id.activityBeerDescription);
        beerUrl = (ImageView) findViewById(R.id.activityBeerIcon);

        tempBeer = new Beer(beerName.getText().toString(),
                beerDescription.getText().toString(),
                beerIconUrl);

        if (getIntent().getExtras() != null) {
            beerId = getIntent().getExtras().getInt("id");
            tempBeer = mydb.getBeersList().get(beerId);
        }

        beerName.setText(tempBeer.getName());
        beerDescription.setText(tempBeer.getDescription());
//        beerUrl.setImageDrawable();

        oldBeerName = beerName.getText().toString();

        // Save Beer Button
        Button saveBeer = (Button) findViewById(R.id.activityBeerSaveButton);
        saveBeer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempBeer = new Beer(beerName.getText().toString(),
                        beerDescription.getText().toString(),
                        beerIconUrl);

                if (beerId == -1)
                    mydb.putBeer(getApplicationContext(), tempBeer);
                else {
                    mydb.updateBeer(oldBeerName, tempBeer);
                }
                Toast.makeText(getApplicationContext(), beerName.getText(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        // Delete Beer Button
        Button deleteBeer = (Button) findViewById(R.id.activityBeerDeleteButton);
        deleteBeer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.deleteBeer(oldBeerName);
                finish();
            }
        });

        // Image Beer Button
        beerUrl.setImageURI(Uri.parse(beerIconUrl));
        beerUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int RESULT_LOAD_IMAGE = 1;
//                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, RESULT_LOAD_IMAGE);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), RESULT_LOAD_IMAGE);
                System.out.println("BEER IMAGE: " + RESULT_LOAD_IMAGE);
            }
        });
    }
}
