package com.example.chello.beeralbum;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class MainBeerAlbumActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private static MainBeerAlbumActivity instance;

    ListView beerListView;
    BeerAdapter arrayAdapter;
//    ArrayAdapter<Beer> arrayAdapter;
//    ArrayList<Beer> beerList = new ArrayList<Beer>();
    BeerAlbumDbHelper mydb;

    public static MainBeerAlbumActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_beer_album);
        mydb = new BeerAlbumDbHelper(this);
        ArrayList<Beer> beerList = mydb.getBeersList();
        System.out.println("BEER ALBUM: BeersList count: " + beerList.size());
        beerListView = (ListView) findViewById(R.id.beerList);
        arrayAdapter = new BeerAdapter(this, R.layout.single_beer_in_list_layout, beerList);
        beerListView.setAdapter(arrayAdapter);
        beerListView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        Log.w("BEER ALBUM: ", "App resumed");
        refreshListView();
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.w("BEER ALBUM: ", "App stopped");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.w("BEER ALBUM: ", "App destroyed");
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle data = new Bundle();
        data.putInt("id", position);
        Intent intent = new Intent(getApplicationContext(), com.example.chello.beeralbum.SingleBeerActivity.class);
        intent.putExtras(data);

        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_beer_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_entry) {
            Intent intent = new Intent(getApplicationContext(), com.example.chello.beeralbum.SingleBeerActivity.class);
            startActivity(intent);
            refreshListView();
        }
        if (id == R.id.action_delete_entries) {
            SQLiteDatabase db = mydb.getWritableDatabase();
            mydb.onUpgrade(db, 1, 1);
            refreshListView();
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshListView() {
        arrayAdapter.clear();
        arrayAdapter.addAll(mydb.getBeersList());
        arrayAdapter.notifyDataSetChanged();
    }
}
