package com.example.chello.beeralbum;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Chello on 2015-06-24.
 */
public class BeerAdapter extends ArrayAdapter<Beer> {
    static class BeerHolder {
        ImageView icon;
        TextView firstLine;
        TextView secondLine;
    }

    Context context;
    int layoutResourceId;
    ArrayList<Beer> beerList = null;

    public BeerAdapter(Context context, int layoutResourceId, ArrayList<Beer> beerList) {
        super(context, layoutResourceId, beerList);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.beerList = beerList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        BeerHolder holder = null;

        if (row != null) holder = (BeerHolder) row.getTag();
        else {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new BeerHolder();
            holder.icon = (ImageView) row.findViewById(R.id.listBeerIcon);
            holder.firstLine = (TextView) row.findViewById(R.id.listBeerName);
            holder.secondLine = (TextView) row.findViewById(R.id.listBeerDescription);

            row.setTag(holder);
        }

        Beer beer = beerList.get(position);
        holder.firstLine.setText(beer.getName());
        holder.secondLine.setText(beer.getDescription());
        holder.icon.setImageResource(R.drawable.beericon);

//        if (beer.getImageUrl() == "null") holder.icon.setImageResource(R.drawable.beericon);
//        else holder.icon.setImageDrawable(...);
        return row;
    }
}
